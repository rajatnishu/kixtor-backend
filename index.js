const express = require('express');
var app = express();
const mongoose = require('mongoose');
const importData = require('./data.json');
const bodyParser = require('body-parser');
var cors = require('cors');
var request=require('request');
var fs = require("fs")
var config = require('./src/config/service')
let port = process.env.PORT || 3000;
app.use(cors());
require('./src/models/user')
const User = mongoose.model('user');
var emailer = require('./src/Routes/mailsender');


require('./src/models/mentors')
const mentors = mongoose.model('mentors')

var url = config.databaseurl
// var url = "mongodb://localhost:27017/demokixto";
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }).then(function () {
  console.log("Connect Datebase");
}, function (err) {
  console.log(err)
})

var adminapi = require('./src/Routes/admin_api')


app.get('/data', (req, res) => {
  res.send(importData)
})


app.listen(port, () => {
  console.log(`Example app is listening on port ${port}`);
})
app.use(bodyParser.json({ limit: '500mb' }));
app.use(bodyParser.urlencoded({ limit: '500mb', extended: true, parameterLimit: 50000000 }));
app.use('/static', express.static('uploads/profile'));


app.post('/addmentor',async function (req, res){
  let base64Data = req.body.profile
  var base64result = base64Data.substr(base64Data.indexOf(',') + 1);
  var buf = await new Buffer.from(base64result, 'base64');
 
  var docPath = ('profile/' + req.body.mentor_image);
  var fileName = ('uploads/' + docPath);
  await fs.writeFile(fileName, buf, 'base64', async function (err) {
    if (err) {
      console.log('File Error:', err)
      res.send({ error: err })
      return
    }
    let newmentor = await new mentors(req.body).save();
  if(newmentor){
    res.send({ status: 200, data: newmentor })
  } else{
    res.send({ status: 400 })
  }
  })
})

app.post("/adduser", async function (req, res) {
  let result = await User.findOne({ email: req.body.email })
  if (!result) {
    let base64Data = req.body.profile_photo
    var base64result = base64Data.substr(base64Data.indexOf(',') + 1);
    var buf = await new Buffer.from(base64result, 'base64');
    var docPath = ('profile/' + req.body.Uploaded_filename);
    var fileName = ('uploads/' + docPath);
    await fs.writeFile(fileName, buf, 'base64', async function (err) {
      if (err) {
        console.log('File Error:', err)
        res.send({ error: err })
        return
      }
      var uniqueId ='KX'+ Math.floor((Math.random() * 1000000) + 1);
      req.body.unique_id = uniqueId
      let addUser = await new User(req.body).save();
      if (addUser) {
        res.send({ status: 200, data: addUser, text: "your account not approved please wait for admin approval!" })
        let template_url = 'src/template/firstMail.html';
        fs.readFile(template_url, async function read(err, bufcontent) {
          var content = (bufcontent || '').toString();
          var mailObject = await emailer.GetMailObject(req.body.email, "Welcome to Kixtor!", content, null, null)
          emailer.sendEmail(mailObject, function(res) {
              console.log(res);
          });
        })
       
      }
    })
  } else {
    res.send({ status: 400 ,data:"Email already exist"})
  }
})

app.post('/updateuser', async function (req, res) {
  if (req.body.addprofile) {
    let base64Data = req.body.addprofile
    var base64result = base64Data.substr(base64Data.indexOf(',') + 1);
    var buf = await new Buffer.from(base64result, 'base64');
    var docPath = ('profile/' + req.body.Uploaded_filename);
    var fileName = ('uploads/' + docPath);
    await fs.writeFile(fileName, buf, 'base64', async function (err) {
      if (err) {
        console.log('File Error:', err)
        res.send({ error: err })
        return
      }
      let result = await User.update(
        { _id: req.body._id },
        {
          $set: {
            name: req.body.name,
            contact_number: req.body.contact_number,
            email: req.body.email,
            password: req.body.password,
            country: req.body.country,
            state: req.body.state,
            gender: req.body.gender,
            Uploaded_filename: req.body.Uploaded_filename,
            strengths: req.body.strengths,
            stage: req.body.stage,
            skills: req.body.skills,
            industry: req.body.industry,
            country_code: req.body.country_code
          }
        }
      )
      if (result) {
        res.send(result)
      }
    })
  } else {
    let result = await User.update(
      { _id: req.body._id },
      {
        $set: {
          name: req.body.name,
          contact_number: req.body.contact_number,
          email: req.body.email,
          password: req.body.password,
          country: req.body.country,
          state: req.body.state,
          gender: req.body.gender,
          Uploaded_filename: req.body.Uploaded_filename,
          strengths: req.body.strengths,
          stage: req.body.stage,
          skills: req.body.skills,
          industry: req.body.industry,
          country_code: req.body.country_code
        }
      }
    )
    if (result) {
      res.send(result)
    }
  }
})

app.post('/getstate', async function (req, res) {
  console.log(req.body.code)
  let day=  request.get('http://battuta.medunes.net/api/region/'+ req.body.code + '/all/?key=b13bd4f6a1a938f79f6f15519d4a518f',function(err,resp,body){
  if(err) //TODO: handle err
  console.log(body)
  console.log(resp)
  if(resp.statusCode === 200 ){
    console.log(body)
    res.send(body)
  } 
});
})

app.post('/sendMail', async function(req,res){
  let template_url = 'src/template/firstMail.html'
    fs.readFile(template_url, async function read(err, bufcontent) {
      var content = (bufcontent || '').toString();
      var mailObject = await emailer.GetMailObject(req.body.email, "FirstMail", content, null, null)
      emailer.sendEmail(mailObject, function(res) {
          console.log(res);
      });
    })
})

app.use('/admin', adminapi)