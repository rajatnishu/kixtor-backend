var mongoose = require('mongoose')
var requestSchema = mongoose.Schema({
    sender_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    receiver_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    status:String,
    created_at: {
        type: String,
        default: Date
    }
});

exports = mongoose.model('request', requestSchema);