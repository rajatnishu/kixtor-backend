var mongoose = require('mongoose')
var MembershipSchema = mongoose.Schema({
    name: String,
    amount:String,
    months:String,
    view_phone:String,
    connect_message:String,
    created_at: {
        type: String,
        default: Date
    }
});

exports = mongoose.model('membership', MembershipSchema);