var mongoose = require('mongoose')
var mentorsSchema = mongoose.Schema({
    mentor_name: String,
    mentor_image: String,
    mentor_post: String,
    mentor_email: String,
    mentor_cost: String,
    country:String,
    created_at: {
        type: String,
        default: Date
    }
});

exports = mongoose.model('mentors', mentorsSchema);