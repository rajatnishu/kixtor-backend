var mongoose = require('mongoose')
var UserSchema = mongoose.Schema({
    name: String,
    contact_number: String,
    email: String,
    password: String,
    country: String,
    state: String,
    gender: String,
    Uploaded_filename: String,
    otp:Number,
    country_code: String,
    userId:String,
    strengths: String,
    stage: [{
        name:String,
        value:Boolean
    }],
    skills: String,
    industry: [],
    provider:String,
    is_verify:{
        type: String,
        default : "notapprove"
    },
    
    is_membership:{
        type: String,
        default : "NonPremium Membership"
    },

    unique_id:String,
    created_at: {
        type: String,
        default: Date
    }
});

exports = mongoose.model('user', UserSchema);