var mongoose = require('mongoose')
var notificationSchema = mongoose.Schema({
    
    user_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    read:{
        type:Boolean,
        default:false
    },
    created_at: {
        type: String,
        default: Date
    }
});

exports = mongoose.model('notification', notificationSchema);