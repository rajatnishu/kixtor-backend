var mongoose = require('mongoose')
var skillsetSchema = mongoose.Schema({
    name: String,
    created_at: {
        type: String,
        default: Date
    }
});

exports = mongoose.model('skillset', skillsetSchema);