var mongoose = require('mongoose')
var cmsSchema = mongoose.Schema({
    content: String,
    type:String,
    created_at: {
        type: String,
        default: Date
    }
});

exports = mongoose.model('cms', cmsSchema);