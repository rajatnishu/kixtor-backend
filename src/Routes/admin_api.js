var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
var mailsender = require('./mailsender.js');
var request = require('request');


require('../models/user')
const User = mongoose.model('user');

require('../models/category')
const Category = mongoose.model('category');

require('../models/skillset')
const skillset = mongoose.model('skillset')

require('../models/membership')
const Membership = mongoose.model('membership')

require('../models/mentors')
const mentors = mongoose.model('mentors')

require('../models/cms')
const CMS = mongoose.model('cms')

require('../models/notification')
const savenotification = mongoose.model('notification')

require('../models/request')
const Request = mongoose.model('request')

router.post('/login', async function (req, res) {
  let loginData = await User.findOne({ email: req.body.email, password: req.body.password, role: "ADMIN" })
  if (loginData) {
    res.send({ status: 200, data: loginData })
  } else {
    res.send({ status: 400, data: "Invalid Email and Password" })
  }
})

router.post('/userlogin', async function (req, res) {
  let loginData = await User.findOne({ email: req.body.email, password: req.body.password, })
  if (loginData) {
    let result = await User.findOne({ email: req.body.email, password: req.body.password, is_verify: "approve" })
    if (result) {
      res.send({ status: 200, data: loginData })
    } else {
      res.send({ status: 600, data: "your account not approved please wait for admin approval!" })
    }
  } else {
    res.send({ status: 400, data: "Invalid Email and Password" })
  }
})


router.post("/deleteuser", async function (req, res) {
  let updatedata = await User.findOneAndRemove({ _id: req.body.id })
  if (updatedata) {
    res.send({ status: 200, data: updatedata })
  }
  else {
    res.send({ status: 400 })
  }
})



router.get("/alluser", async function (req, res) {
  let getData = await User.find({});
  res.send({ status: 200, data: getData })
})



router.post("/userbyid", async function (req, res) {
  let getData = await User.findById({ _id: req.body.id });
  if (getData) {
    res.send({ status: 200, data: getData })
  } else {
    res.send({ status: 400 })
  }
})

router.post("/deleteskills", async function (req, res) {
  let updatedata = await skillset.findOneAndRemove({ _id: req.body.id })
  if (updatedata) {
    res.send({ status: 200, data: updatedata })
  }
  else {
    res.send({ status: 400 })
  }
})

router.get("/allskill", async function (req, res) {
  let getData = await skillset.find({});
  res.send({ status: 200, data: getData })
})

router.post("/deletecategory", async function (req, res) {
  let updatedata = await Category.findOneAndRemove({ _id: req.body.id })
  if (updatedata) {
    res.send({ status: 200, data: updatedata })
  }
  else {
    res.send({ status: 400 })
  }
})

router.get("/allcategory", async function (req, res) {
  let getData = await Category.find({});
  res.send({ status: 200, data: getData })
})


router.post('/updatecategory', async function (req, res) {
  let result = await Category.update(
    { _id: req.body._id },
    {
      $set: {
        name: req.body.name
      }
    }
  )
  if (result) {
    res.send(result)
  }
})


router.post('/updateskill', async function (req, res) {
  let result = await skillset.update(
    { _id: req.body._id },
    {
      $set: {
        name: req.body.name
      }
    }
  )
  if (result) {
    res.send(result)
  }
})

router.post("/approveaccount", async function (req, res) {
  let data = await User.update(
    { _id: req.body._id },
    {
      $set: {
        is_verify: req.body.is_verify
      }
    }
  )
  if (data) {
    res.send({ status: 200, result: data })
  }
})

router.post("/addskill", async function (req, res) {
  let addCategory = await new skillset(req.body).save();
  if (addCategory) {
    res.send({ status: 200, data: addCategory })
  } else {
    res.send({ status: 400 })
  }

})


router.post("/addcategory", async function (req, res) {
  let addCategory = await new Category(req.body).save();
  if (addCategory) {
    res.send({ status: 200, data: addCategory })
  } else {
    res.send({ status: 400 })
  }
})

router.post('/requestemail', async function (req, res) {
  let userdata = await User.findOne({ email: req.body.email })
  if (userdata) {
    req.body.otp = Math.floor(1000 + Math.random() * 9000);
    let result = await User.update(
      { email: req.body.email },
      {
        $set: {
          otp: req.body.otp,
        }
      }
    )
    if (result) {
      mailsender.sendotp(req.body, async (info) => {
        res.send({ status: 200, result: userdata })
      })
    }
  } else {
    res.send({ status: 400 })
  }
})

router.post("/updatepassword", async function (req, res) {
  let result = await User.findOneAndUpdate(
    { email: req.body.email, otp: req.body.otp },
    {
      $set: {
        password: req.body.newpassword
      }
    }
  )
  if (result) {
    res.send({ status: 200, data: result })
  } else {
    res.send({ status: 400 })
  }
})




router.get('/allmentor', async function (req, res) {
  let mentor = await mentors.find().sort({ $natural: -1 })
  if (mentor) {
    res.send({ status: 200, result: mentor })
  } else {
    res.send({ status: 400 })
  }
})

router.post('/mentor', async function (req, res) {
  let mentor = await mentors.find().sort({ $natural: -1 }).limit(5)
  if (mentor) {
    res.send({ status: 200, result: mentor })
  } else {
    res.send({ status: 400 })
  }
})


router.post('/deletementor', async function (req, res) {
  let result = await mentors.remove({ _id: req.body._id })
  if (result) {
    res.send({ status: 200, res: result })
  } else {
    res.send({ status: 400 })
  }
})


router.post("/mentorbyid", async function (req, res) {
  let getData = await mentors.findById({ _id: req.body.id });
  if (getData) {
    res.send({ status: 200, data: getData })
  } else {
    res.send({ status: 400 })
  }
})

router.post('/updatementor', async function (req, res) {
  let result = await mentors.update(
    { _id: req.body._id },
    {
      $set: {
        mentor_email: req.body.mentor_email,
        mentor_post: req.body.mentor_post,
        mentor_name: req.body.mentor_name,
        mentor_cost: req.body.mentor_cost,
        mentor_image: req.body.mentor_image,
        country: req.body.country
      }
    }
  )
  if (result) {
    res.send(result)
  }
})

router.post("/addmembership", async function (req, res) {
  let result = await Membership.find({ name: req.body.name })
  if (result.length) {
    res.send({ status: 400 })
  }
  else {
    let addMembership = await new Membership(req.body).save();
    if (addMembership) {
      res.send({ status: 200, data: addMembership })
    } else {
      res.send({ status: 400 })
    }
  }

})

router.get("/allmembership", async function (req, res) {
  let getData = await Membership.find({});
  res.send({ status: 200, data: getData })
})

router.post('/updatemembership', async function (req, res) {
  let findMembership = await Membership.find({ name: req.body.name, _id: { $ne: req.body._id } });
  if (findMembership && findMembership.length) {
    res.send({ status: 400 })
  } else {
    let result = await Membership.update(
      { _id: req.body._id },
      {
        $set: {
          name: req.body.name,
          amount: req.body.amount,
          months: req.body.months,
          view_phone: req.body.view_phone,
          connect_message: req.body.connect_message
        }
      }
    )
    if (result) {
      res.send({ status: 200, data: result })
    }
  }
})

router.post("/deletemembership", async function (req, res) {
  let deletedata = await Membership.findOneAndRemove({ _id: req.body.id })
  if (deletedata) {
    res.send({ status: 200, data: deletedata })
  }
  else {
    res.send({ status: 400 })
  }
})

router.post("/membershipbyid", async function (req, res) {
  let getData = await Membership.findById({ _id: req.body.id });
  if (getData) {
    res.send({ status: 200, data: getData })
  } else {
    res.send({ status: 400 })
  }
})

router.post("/allcms", async function (req, res) {
  let getData = await CMS.findOne({ type: req.body.type });
  res.send({ status: 200, data: getData })
})

router.post("/addcms", async function (req, res) {
  let addCms = await new CMS(req.body).save();
  if (addCms) {
    res.send({ status: 200, data: addCms })
  } else {
    res.send({ status: 400 })
  }
})

router.post("/addnotifaction", async function (req, res) {
  let newnotification = await new savenotification(req.body).save();
  if (newnotification) {
    res.send({ status: 200, data: newnotification })
  } else {
    res.send({ status: 400 })
  }
})

router.get('/allnotification', async function (req, res) {
  let allnotification = await savenotification.find().sort({ $natural: -1 }).limit(3).populate('user_id')
  if (allnotification) {
    res.send({ status: 200, data: allnotification })
  }
  else {
    res.send({ status: 400 })
  }
})

router.post('/userdata', async function (req, res) {
  let loguser = await User.findOne({ _id: req.body.email })
  if (loguser) {
    res.send({ status: 200, data: loguser })
  } else {
    res.send({ status: 400 })
  }
})

router.post('/updatecms', async function (req, res) {
  let result = await CMS.update(
    { _id: req.body._id },
    {
      $set: {
        content: req.body.content
      }
    }
  )
  if (result) {
    res.send(result)
  }
})

router.post('/searchuser', async function (req, res) {
  let query = {};
  // if(req.body.country){

  // } else if() {

  // } else if() {

  // }

  let result = await User.find({ country: req.body.country, _id: { $ne: req.body.user_id }, unique_id: req.body.kixtor_id });
  if (result) {
    res.send({ status: 200, data: result })
  } else {
    res.send({ status: 400 })
  }
});

router.post('/sendrequesttouser', async function (req, res) {
  let sendResult = await new Request(req.body).save();
  if (sendResult) {
    res.send({ status: 200, data: sendResult })
  } else {
    res.send({ status: 400 })
  }
})

router.get('/allpendingrequest/:id', async function (req, res) {
  let result = await Request.find({ receiver_id: req.params.id, status: "SEND" }).populate('sender_id');
  if (result) {
    res.send({ status: 200, data: result })
  } else {
    res.send({ status: 400 })
  }
});

router.get('/allsentrequest/:id', async function (req, res) {
  let result = await Request.find({ sender_id: req.params.id, status: "SEND" }).populate('receiver_id');
  if (result) {
    res.send({ status: 200, data: result })
  } else {
    res.send({ status: 400 })
  }
});

router.get('/allacceptrequest/:id', async function (req, res) {
  let result = await Request.find({ sender_id: req.params.id, status: "SEND" }).populate('sender_id receiver_id');
  if (result) {
    res.send({ status: 200, data: result })
  } else {
    res.send({ status: 400 })
  }
});

router.get('/acceptrequest/:id', async function (req, res) {
  let result = await Request.update(
    { _id: req.params.id },
    {
      $set: {
        status: 'ACCEPT'
      }
    }
  )
  if (result) {
    res.send(result)
  }
});


router.post('/googleLogin',async function(req,res){
  let findUser = await User.findOneAndDelete({email:req.body.email,password:"12345"});
  if(findUser){
     res.send({status:200,data:findUser})
  } else {
     let data ={
       name:req.body.name,
       email:req.body.email,
       password:"12345",
       Uploaded_filename:req.body.photoUrl,
       unique_id:'KX'+ Math.floor((Math.random() * 1000000) + 1),
       provider:req.body.provider
     }
     let addUser = await new User(data).save();
      if (addUser) {
        res.send({status:200,data:addUser});
      } else {
        res.send({status:400});
      }
  }
})

module.exports = router