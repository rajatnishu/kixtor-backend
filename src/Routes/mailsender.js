var nodemailer = require('nodemailer');
var config = require('../config/service');

let smtpConfig = {
    host: "smtp.gmail.com",
    port: config.mail_port,
    secure: false,
    auth: {
        user: config.username,
        pass: config.password,
    }
}

let transporter = nodemailer.createTransport(smtpConfig);

exports.sendotp = async function (userData, callback) {


    let mailOptions = {
        from: config.username,
        to: userData.email,
        subject: 'User send File URL',
        text: "this otp valid 30 min " + " " + userData.otp
    };
    let info = await transporter.sendMail(mailOptions);
    callback(info);
}

exports.GetMailObject = function (to, subject, html, cc, bcc) {

    function MailException(message) {
        this.message = message;
        this.name = 'MailException';
    }

    var mailObject = {};

    if (to)
        mailObject.to = to;
    else
        throw new MailException("To filed is maindatory");

    if (subject)
        mailObject.subject = subject;
    else
        throw new MailException("Subject is maindatory");

    if (html)
        mailObject.html = html;
    else
        throw new MailException("Body is maindatory");

    if (cc)
        mailObject.cc = cc;

    if (bcc)
        mailObject.bcc = bcc;

    return mailObject;
}

exports.sendEmail = function (contents, cb) {
    contents.from = config.username
    return transporter.sendMail(contents, function (error, info) {
        if (error) {
            console.log(error);
            cb({
                mailsuccess: false,
                data: null
            });
        } else
            cb({
                mailsuccess: true,
                data: info
            });
    });
}